"use strict";

// Практичне завдання:

// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
//  При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція,
//  написана в джаваскрипті, через такі правки не переставала працювати.

//  Умови:
//  - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
document.addEventListener("DOMContentLoaded", function () {
  const tabs = document.querySelectorAll(".tabs-title");
  const tabContent = document.querySelectorAll(".tabs-content li");

  function changeTab(e) {
    const targetTab = e.target;
    const tabNumber = Array.from(tabs).indexOf(targetTab);

    if (tabNumber === -1) return;

    tabs.forEach((tab) => tab.classList.remove("active"));
    tabContent.forEach((content) => content.classList.remove("active"));

    targetTab.classList.add("active");
    tabContent[tabNumber].classList.add("active");
  }

  document.querySelector(".tabs").addEventListener("click", changeTab);
});
